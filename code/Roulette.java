import java.util.Scanner;

public class Roulette 
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        RouletteWheel rouletteWheel = new RouletteWheel();
        final int startingMoneyAmount = 1000;
        int playerMoneyAmount = startingMoneyAmount;
        boolean endGame = false;

        System.out.println("Welcome to the Roulette game!");
        
        while (!endGame)
        {
            playerMoneyAmount = playARound(rouletteWheel, scan, playerMoneyAmount);
            if (playerMoneyAmount <= 0)
            {
                System.out.println("\nYou have no more money to bet with. The game will end here.");
                break;
            }
            System.out.println("\nWould you like to end the game? Choose 0 for yes and 1 for no.");
            int endGameInput = askInput(scan, 0, 1);
            endGame = endGameInput == 0;
        }
        String endingText = playerMoneyAmount >= startingMoneyAmount ? "won" : "lost";
        System.out.println("\nYou have "+endingText+" "+Math.abs(playerMoneyAmount-startingMoneyAmount)+"$.");
        scan.close();
    }

    private static int playARound(RouletteWheel rouletteWheel, Scanner scan, int playerMoneyAmount)
    {
        System.out.println("\nWhat is the number you want to bet on?");
        int inputNumber = askInput(scan, 0, 36);

        System.out.println("\nHow much would you like to bet? You currently have "+playerMoneyAmount+"$.");
        int inputBettingAmount = askInput(scan, 1, playerMoneyAmount);

        rouletteWheel.spin();
        int landingNumber = rouletteWheel.getNumberValue();
        System.out.println("\nThe roulette wheel have been spinned! The number is: "+landingNumber+".");

        if (inputNumber == landingNumber)
        {
            playerMoneyAmount += (inputBettingAmount*35);
            System.out.println((inputBettingAmount*35)+"$ have been added to your money for choosing the correct number.");
        }
        else
        {
            playerMoneyAmount -= inputBettingAmount;
            System.out.println((inputBettingAmount)+"$ have been removed from your money for choosing the wrong number.");
        }

        return playerMoneyAmount;
    }

    private static int askInput(Scanner scan, int minInt, int maxInt)
    {
        int input = 0;
        do {
            System.out.println("Make sure to answer with an integer ranging from "+minInt+" to "+maxInt+".");
            input = scan.nextInt();
            scan.nextLine();
        } while (input < minInt || input > maxInt);
        return input;
    }
}